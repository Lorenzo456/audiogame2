﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Song : MonoBehaviour {

    AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();

    }

    IEnumerator StartSong(AudioClip audioClip)
    {
        yield return new WaitForSeconds(1);
        audioSource.Play();
    }

}
