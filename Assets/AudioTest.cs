﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTest : MonoBehaviour {

    AudioSource aud; 
    float[] samples;

    // Use this for initialization
    void Start()
    {
        aud = GetComponent<AudioSource>();
        samples = new float[aud.clip.samples * aud.clip.channels];
        aud.clip.GetData(samples, 0);

        int i = 0;
        while (i < samples.Length)
        {
            samples[i] = samples[i] * 0.5F;
            ++i;
        }
        aud.clip.SetData(samples, 0);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
