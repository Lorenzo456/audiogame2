﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class interactable : MonoBehaviour {

    public float speed;
    MeshRenderer meshRenderer;
    ParticleSystem particleSystem;
    public bool holdInteraction;
    BoxCollider boxCollider;

    private void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        particleSystem = GetComponent<ParticleSystem>();
        boxCollider = GetComponent<BoxCollider>();
    }

    // Update is called once per frame
    void Update ()
    {
        float translation = Time.deltaTime * speed;
        transform.Translate(0,-translation,0);     
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PlayerCube"))
        {
            if (!holdInteraction && !other.GetComponent<HitScript>().isPressedDown)
            {
                Gamemanager.instance.score += (int)(10 / Vector3.Distance(transform.position, other.transform.position));
                particleSystem.Play();
                meshRenderer.enabled = false;
                boxCollider.enabled = false;

            }
            else if (holdInteraction && other.GetComponent<HitScript>().isPressedDown || holdInteraction && other.GetComponent<HitScript>().isPressed)
            {
                Gamemanager.instance.score += (int)(10 / Vector3.Distance(transform.position, other.transform.position));
                particleSystem.Play();
                meshRenderer.enabled = false;
                boxCollider.enabled = false;
            }


        }

        if (other.CompareTag("Destroy"))
        {
            Destroy(gameObject);
        }

        if (other.CompareTag("Interactable"))
        {
            if (!holdInteraction)
            {
                holdInteraction = true;
                meshRenderer.material.color = Color.gray;
            }
        }

        /*
        if (other.CompareTag("PlayerCube"))
        {
            if (holdInteraction && Gamemanager.instance.GetComponent<HitTriggerScript>().isPressedDown || holdInteraction && Gamemanager.instance.GetComponent<HitTriggerScript>().pressedBefore)
            {
                //Debug.Log(Vector3.Distance(transform.position, other.transform.position));
                Gamemanager.instance.score += (int)(10 / Vector3.Distance(transform.position, other.transform.position));
                Destroy(gameObject);
            }
            else if (!holdInteraction && !Gamemanager.instance.GetComponent<HitTriggerScript>().isPressedDown)
            {
                //Debug.Log(Vector3.Distance(transform.position, other.transform.position));
                Gamemanager.instance.score += (int)(10 / Vector3.Distance(transform.position, other.transform.position));
                Destroy(gameObject);
            }

        }
        */




    }
}
