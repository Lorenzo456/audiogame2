﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jumppad : MonoBehaviour, NoteInteraction
{

    public bool jumpForce;

    private Rigidbody rb;

    private Material material;
    Color color1 = Color.blue;
    Color color2 = Color.black;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        material = GetComponent<Renderer>().material;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (jumpForce)
        {
            
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, 2, 1 << 8);
            
            for (int i = 0; i < hitColliders.Length; i++)
            {
                if (hitColliders[i].CompareTag("Player"))
                {
                    Debug.Log("PLAYER IS HERE");
                    hitColliders[i].GetComponent<Rigidbody>().AddForce(hitColliders[i].transform.up * 500);
                }
            }
            material.color = color1;
        }
        else
        {
            material.color = color2;

        }
    }

    public void Interaction()
    {
        Debug.Log("JUMP ONN");
        jumpForce = true;
    }

    public void InteractionOff()
    {
        Debug.Log("JUMP OFF");
        jumpForce = false;
    }

}
