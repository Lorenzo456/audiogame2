﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destroyNote : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Destroy"))
        {
            other.gameObject.SetActive(false);
        }
    }
}
