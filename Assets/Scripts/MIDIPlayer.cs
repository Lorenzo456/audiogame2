﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CSharpSynth.Effects;
using CSharpSynth.Sequencer;
using CSharpSynth.Synthesis;
using CSharpSynth.Midi;

[RequireComponent(typeof(AudioSource))]
public class MIDIPlayer : MonoBehaviour
{
    //Public
    //Check the Midi's file folder for different songs
    public string midiFilePath = "Midis/Groove.mid";
    public bool ShouldPlayFile = true;
    public AudioSource audioPlayer;

    //Try also: "FM Bank/fm" or "Analog Bank/analog" for some different sounds
    //geluid per channel
    public string bankFilePath = "GM Bank/gm";
    public int bufferSize = 1024;
    public int midiNote = 60; //= C
    public int midiNoteVolume = 100;
    [Range(0, 127)] //From Piano to Gunshot
    public int midiInstrument = 0;

    //
    public jumppad beatSpawner;
    public int beatChannel;
    public int note1;
    public jumppad beatSpawner2;
    public int beatChannel2;
    public int note2;
    public jumppad beatSpawner3;
    public int beatChannel3;
    public int note3;
    public jumppad beatSpawner4;
    public int beatChannel4;
    public int note4;

    public bool detectBeat;

    //Private 
    private float[] sampleBuffer;
    private float gain = 1f;
    private MidiSequencer midiSequencer;
    private StreamSynthesizer midiStreamSynthesizer;

    private float sliderValue = 1.0f;
    private float maxSliderValue = 127.0f;
    private AudioSource audioSource;
    // Awake is called when the script instance
    // is being loaded.
    public float delayMIDI = 2f;
    public float delayMusic = .75f;
    public bool playSimultaneously;

    void Awake()
    {

        midiStreamSynthesizer = new StreamSynthesizer(44100, 2, bufferSize, 40);
        sampleBuffer = new float[midiStreamSynthesizer.BufferSize];
        
        midiStreamSynthesizer.LoadBank(bankFilePath);

        midiSequencer = new MidiSequencer(midiStreamSynthesizer);

        if (detectBeat)
        {
            audioSource = GetComponent<AudioSource>();
           // audioSource.Play();
            StartCoroutine(PlayMusic());
            //These will be fired by the midiSequencer when a song plays. Check the console for messages if you uncomment these
            midiSequencer.NoteOnEvent += new MidiSequencer.NoteOnEventHandler(MidiNoteOnHandler);
            midiSequencer.NoteOffEvent += new MidiSequencer.NoteOffEventHandler(MidiNoteOffHandler);
            //Invoke("PlayHearableMusic", soundOffset);
        }

    }

    IEnumerator PlayMusic()
    {
        if (playSimultaneously)
        {
            audioSource.Play();
            audioPlayer.Play();
            yield return null;
        }
        yield return new WaitForSeconds(delayMIDI);
        audioSource.Play();
        yield return new WaitForSeconds(delayMusic);
        audioPlayer.Play();
    }
    public void PlayHearableMusic()
    {
        audioPlayer.Play();
    }

    void LoadSong(string midiPath)
    {
        midiSequencer.LoadMidi(midiPath, false);
        midiSequencer.Play();
    }

    // Start is called just before any of the
    // Update methods is called the first time.
    void Start()
    {
    }

    // Update is called every frame, if the
    // MonoBehaviour is enabled.
    void Update()
    {
        if (!midiSequencer.isPlaying)
        {
            //if (!GetComponent<AudioSource>().isPlaying)
            if (ShouldPlayFile)
            {
                LoadSong(midiFilePath);
            }
        }
        else if (!ShouldPlayFile)
        {
            midiSequencer.Stop(true);
        }
        /*
        if (Input.GetButtonDown("Fire1"))
        {
            midiStreamSynthesizer.NoteOn(0, midiNote, midiNoteVolume, midiInstrument);
        }

        if (Input.GetButtonUp("Fire1"))
        {
            midiStreamSynthesizer.NoteOff(0, midiNote);
        }
        */

    }

    // See http://unity3d.com/support/documentation/ScriptReference/MonoBehaviour.OnAudioFilterRead.html for reference code
    //	If OnAudioFilterRead is implemented, Unity will insert a custom filter into the audio DSP chain.
    //
    //	The filter is inserted in the same order as the MonoBehaviour script is shown in the inspector. 	
    //	OnAudioFilterRead is called everytime a chunk of audio is routed thru the filter (this happens frequently, every ~20ms depending on the samplerate and platform). 
    //	The audio data is an array of floats ranging from [-1.0f;1.0f] and contains audio from the previous filter in the chain or the AudioClip on the AudioSource. 
    //	If this is the first filter in the chain and a clip isn't attached to the audio source this filter will be 'played'. 
    //	That way you can use the filter as the audio clip, procedurally generating audio.
    //
    //	If OnAudioFilterRead is implemented a VU meter will show up in the inspector showing the outgoing samples level. 
    //	The process time of the filter is also measured and the spent milliseconds will show up next to the VU Meter 
    //	(it turns red if the filter is taking up too much time, so the mixer will starv audio data). 
    //	Also note, that OnAudioFilterRead is called on a different thread from the main thread (namely the audio thread) 
    //	so calling into many Unity functions from this function is not allowed ( a warning will show up ). 	
    private void OnAudioFilterRead(float[] data, int channels)
    {
        //This uses the Unity specific float method we added to get the buffer
        midiStreamSynthesizer.GetNext(sampleBuffer);

        for (int i = 0; i < data.Length; i++)
        {
            data[i] = sampleBuffer[i] * gain;
        }
    }

    public void MidiNoteOnHandler(int channel, int note, int velocity)
    {
        //Debug.Log("Channel:" + channel.ToString() + " NoteOn: " + note.ToString() + " Velocity: " + velocity.ToString());

        if(beatSpawner == null)
            return;

        if (channel == beatChannel && note == note1)
        {
            //Debug.Log("Piano");
          //  Debug.Log("Channel:" + channel.ToString() + " NoteOn: " + note.ToString() + " Velocity: " + velocity.ToString());
            beatSpawner.Interaction();

        }

        if (beatSpawner2 == null)
            return;
        if (channel == beatChannel2 && note == note2)
        {
            // Debug.Log("GUITAR");
           // Debug.Log("Channel:" + channel.ToString() + " NoteOn: " + note.ToString() + " Velocity: " + velocity.ToString());
            beatSpawner2.Interaction();
        }
        if (beatSpawner3 == null)
            return;
        if (channel == beatChannel3 && note == note3)
        {
            //Debug.Log("Piano");
            //Debug.Log("Channel:" + channel.ToString() + " NoteOn: " + note.ToString() + " Velocity: " + velocity.ToString());
            beatSpawner3.Interaction();
        }
        if (beatSpawner4 == null)
            return;
        if (channel == beatChannel4 && note == note4)
        {
            // Debug.Log("GUITAR");
           // Debug.Log("Channel:" + channel.ToString() + " NoteOn: " + note.ToString() + " Velocity: " + velocity.ToString());
            beatSpawner4.Interaction();
        }

        /*
        if (note == 48 && channel == 2)
        {
            Debug.Log("GUITAR 2");
        }
        */
        //Debug.Log("Channel:" + channel.ToString() + " NoteOn: " + note.ToString() + " Velocity: " + velocity.ToString());
    }

    public void MidiNoteOffHandler(int channel, int note)
    {

        if (beatSpawner == null)
            return;

        if (channel == beatChannel && note == note1)
        {
            //Debug.Log("Piano");
            //  Debug.Log("Channel:" + channel.ToString() + " NoteOn: " + note.ToString() + " Velocity: " + velocity.ToString());
            beatSpawner.InteractionOff();
        }

        if (channel == beatChannel2 && note == note2)
        {
            //Debug.Log("Piano");
            //  Debug.Log("Channel:" + channel.ToString() + " NoteOn: " + note.ToString() + " Velocity: " + velocity.ToString());
            beatSpawner2.InteractionOff();
        }
        if (channel == beatChannel3 && note == note3)
        {
            //Debug.Log("Piano");
            //  Debug.Log("Channel:" + channel.ToString() + " NoteOn: " + note.ToString() + " Velocity: " + velocity.ToString());
            beatSpawner3.InteractionOff();
        }
        if (channel == beatChannel4 && note == note4)
        {
            //Debug.Log("Piano");
            //  Debug.Log("Channel:" + channel.ToString() + " NoteOn: " + note.ToString() + " Velocity: " + velocity.ToString());
            beatSpawner4.InteractionOff();
        }
        /*
        if (note == 67)
        {
            Debug.Log("BEATEND");
        }
        Debug.Log("NoteOff: " + note.ToString());
        */
    }
}
