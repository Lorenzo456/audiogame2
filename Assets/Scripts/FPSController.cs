﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSController : MonoBehaviour {

    public float speed = 2f;
    public float originalSpeed;
    public float aimSpeedMultiplyer = 1;
    public float crouchSpeedMultiplyer = 1;
    public float currentSpeed;
    public float sensitivity = 2f;
    public GameObject Eyes;


    float moveFB;
    float moveLR;
    float rotationX;
    float rotationY;

    
    CharacterController player;
    Animator animator;

	void Start () {
        player = GetComponent<CharacterController>();
        animator = GetComponentInChildren<Animator>();
        originalSpeed = speed;
	}

    private void Update()
    {
        Movement();
    }


    void SwapWeapon()
    {
        if (Player.instance.weapons.Length > 0)
        {
            for (int i = 0; i < Player.instance.weapons.Length; i++)
            {
                Player.instance.weapons[i].SetActive(false);
            }

            if (Player.instance.currentWeapon >= Player.instance.weapons.Length -1)
            {
                Player.instance.currentWeapon = 0;
            }
            else
            {
                Player.instance.currentWeapon++;
            }
            animator.SetTrigger("SwapWeapon");
        }
    }

    void CameraMovement()
    {
        rotationX = Input.GetAxis("Mouse X") * sensitivity;
        transform.Rotate(0, rotationX, 0) ;
        
        rotationY += Input.GetAxis("Mouse Y") * sensitivity;

        rotationY = Mathf.Clamp(rotationY, -40, 40);
        Eyes.transform.eulerAngles = new Vector3(-rotationY, Eyes.transform.eulerAngles.y, 0);
        
    }

    void Movement() {

        moveFB = Input.GetAxis("Vertical") * speed;
        moveLR = Input.GetAxis("Horizontal") * speed;
        


        Vector3 movement = new Vector3(moveLR, 0, moveFB);
        movement = Vector3.Normalize(transform.rotation * movement);

        currentSpeed = speed * aimSpeedMultiplyer * crouchSpeedMultiplyer;


        player.Move(movement * currentSpeed * Time.deltaTime);

        // Eyes.transform.Rotate(-rotationY,0, 0);
        CameraMovement();

    }
}
