﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    public GameObject[] weapons;
    public GameObject weaponHolder;
    public int currentWeapon = 0;
    public static Player instance = null;

    public GameObject feedBackReticle;
    bool feedBackhit;

    public bool crouching;
    public int Health;
    public int currentHealth;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

    }

    // Use this for initialization
    void Start () {
        currentHealth = Health;
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void OnFeedBackHit(int health = 1)
    {
        feedBackhit = true;
        feedBackReticle.SetActive(true);
        if(health <= 0)
        {
            feedBackReticle.GetComponentInChildren<Image>().color = Color.red;
        }
        else
        {
            feedBackReticle.GetComponentInChildren<Image>().color = Color.white;

        }
        // Debug.Log("GOTHIT");
        StartCoroutine("OnFeedBackCheck");
    }
     IEnumerator OnFeedBackCheck()
    {
        yield return new WaitForSeconds(.1f);
        feedBackhit = false;
//        feedBackReticle.SetActive(true);
        yield return new WaitForSeconds(.1f);

        if (feedBackhit)
        {
         //   Debug.Log("RETURN GOT HIT AGAIN");
            yield return null;
        }
        else
        {
       //     Debug.Log("DISABLE FEEDBACK");
            feedBackReticle.SetActive(false);
        }
    }

    public void OnHit(int damageTaken = 0)
    {
        currentHealth -= damageTaken;
    }

    public int CheckHealth()
    {
        return currentHealth;
    }
}
