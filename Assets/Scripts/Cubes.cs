﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cubes : MonoBehaviour {

    public GameObject sampleCubePrefab;
    public float maxScale;
    GameObject[] sampleCube = new GameObject[50];

    // Use this for initialization
    void Start()
    {
        /*
        for (int i = 0; i < sampleCube.Length; i++)
        {
            GameObject instanceSampleCube = Instantiate(sampleCubePrefab) as GameObject;
            instanceSampleCube.transform.position = this.transform.position;
            instanceSampleCube.transform.parent = this.transform;
            instanceSampleCube.name = "sampleCube " + i;
            this.transform.eulerAngles = new Vector3(0, -0.703125f * i, 0);
            instanceSampleCube.transform.position = Vector3.forward * 100;
            sampleCube[i] = instanceSampleCube;
        }
        */

        for (var i = 0; i < sampleCube.Length; i++)
        {

            GameObject instance = Instantiate(sampleCubePrefab, transform.position + new Vector3(i,0,0), Quaternion.identity) as GameObject;
            sampleCube[i] = instance;
        }
    }
	// Update is called once per frame
	void Update ()
    {
        for (int i = 0; i < sampleCube.Length; i++)
        {
            if(sampleCube != null)
            {
                sampleCube[i].transform.localScale = new Vector3(10, (AudioStart.samples[i] * maxScale) + 2, 10);
            }
        }


    }
}
