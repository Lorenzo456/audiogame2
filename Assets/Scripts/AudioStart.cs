﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioStart : MonoBehaviour {

    AudioSource audioSource;
    public static float[] samples = new float[512];
    public static float[] feqBand = new float[8];
    public static float[] bandBuffer = new float[8];
    float[] bufferDecrease = new float[8];
    float[] feqhighest = new float[8];
    public static float[] audioBand = new float[8];
    public static float[] audioBandBuffer = new float[8];
    public GameObject song;

    // Use this for initialization
    void Start ()
    {
        audioSource = GetComponent<AudioSource>();
        Debug.Log(audioSource.clip);
        song.GetComponent<Song>().StartCoroutine("StartSong", audioSource.clip);
        //audioSource.clip.GetData(samples, 0);
       // Debug.Log(audioSource.clip.GetData(samples, 0));

	}

	
	// Update is called once per frame
	void Update () {

            GetSpectrumAudioSource();
            MakeFrequencyBand();
            BandBuffer();
	}

    void CreateAudioBand()
    {
        for (int i = 0; i < 8; i++)
        {
            if(feqBand[i] > feqhighest[i])
            {
                feqhighest[i] = feqBand[i];
            }
            audioBand[i] = (feqBand[i] / feqhighest[i]);
            audioBandBuffer[i] = (bandBuffer[i] / feqhighest[i]);

        }

    }
    void BandBuffer()
    {
        for (int i = 0; i < 8; i++)
        {
            if(feqBand[i] > bandBuffer[i])
            {
                bandBuffer[i] = feqBand [i];
                bufferDecrease[i] = 0.005f;
            }

            if (feqBand[i] < bandBuffer[i])
            {
                bandBuffer[i] -= bufferDecrease[i];
                bufferDecrease[i] *= 1.2f;
            }
        }
    }
    void MakeFrequencyBand()
    {
        /*
         22050 / 512 = 43 hertz per sample
         1
         
         */
        int count = 0;
        for (int i = 0; i < 4; i++)
        {

            float average = 0;
            int sampleCount;// = (int)Mathf.Pow(2, i) * 2;

            if (i == 0)
            {
                sampleCount = 8;
            }else if (i == 1)
            {
                sampleCount = 16;
            }
            else if (i == 2)
            {
                sampleCount = 64;
            }
            else if (i == 3)
            {
                sampleCount = 128;
            }
            else if (i == 4)
            {
                sampleCount = 256;
            }
            else
            {
                sampleCount = 0;
            }

            for(int j = 0; j < sampleCount; j++)
            {
                average += samples[count] * (count + 1);
                count++;
            }

            average /= count;
            feqBand[i] = average * 10;
          //  Debug.Log("feqBand: " + i + " : " + feqBand[i]);
        }
    }
    void GetSpectrumAudioSource()
    {
        audioSource.GetSpectrumData(samples,0, FFTWindow.Blackman);
    }
}
