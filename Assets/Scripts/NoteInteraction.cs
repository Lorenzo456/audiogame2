﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface NoteInteraction
{
    void Interaction();
    void InteractionOff();
}
