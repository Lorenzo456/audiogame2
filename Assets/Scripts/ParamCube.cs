﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParamCube : MonoBehaviour {

    public int band;
    public float startScale, scaleMultiplier, beatRate;
    public bool useBuffer;
    public Color color;
    MeshRenderer meshRenderer;
    public GameObject interactable;


	// Use this for initialization
	void Start () {
        meshRenderer = GetComponent<MeshRenderer>();
	}
	
	// Update is called once per frame
	void Update () {


        if (!useBuffer)
        {
            transform.localScale = new Vector3(transform.localScale.x, (AudioStart.feqBand[band] * scaleMultiplier) + startScale, transform.localScale.z);
        }
        else
        {
            transform.localScale = new Vector3(transform.localScale.x, (AudioStart.bandBuffer[band] * scaleMultiplier) + startScale, transform.localScale.z);
        }

        if (AudioStart.feqBand[band]  > beatRate)
        {
            meshRenderer.material.color = color;
            GameObject temp = Instantiate(interactable, transform.position, Quaternion.identity);
            temp.GetComponent<MeshRenderer>().material.color = color;
        }
        else
        {
            meshRenderer.material.color = Color.black;

        }
    }
}
