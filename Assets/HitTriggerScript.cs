﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitTriggerScript : MonoBehaviour {
    public bool keyA;
    public bool keyS;
    public bool keyD;
    public bool keyF;

    public GameObject CubeA;
    public GameObject CubeS;
    public GameObject CubeD;
    public GameObject CubeF;


    public bool pressedBefore;
    public bool isPressedDown;

    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.A)) 
        {
            Debug.Log("A");
            keyA = true;
            StartCoroutine(TurnOnCollider(CubeA, KeyCode.A));
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            Debug.Log("S");
            StartCoroutine(TurnOnCollider(CubeS, KeyCode.S));
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            Debug.Log("D");
            StartCoroutine(TurnOnCollider(CubeD, KeyCode.D));
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            Debug.Log("F");
            StartCoroutine(TurnOnCollider(CubeF, KeyCode.F));
        }


        if (Input.GetKeyUp(KeyCode.A))
        {
            TurnOffCollider(CubeA, KeyCode.A);
            isPressedDown = false;
            keyA = false;
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            TurnOffCollider(CubeS, KeyCode.S);
            isPressedDown = false;

        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            TurnOffCollider(CubeD, KeyCode.D);
            isPressedDown = false;

        }
        if (Input.GetKeyUp(KeyCode.F))
        {
            TurnOffCollider(CubeF, KeyCode.F);
            isPressedDown = false;

        }
    }

    void TurnOffCollider(GameObject trigger, KeyCode keyCode)
    {
        trigger.GetComponent<BoxCollider>().enabled = false;
        
    }
    IEnumerator TurnOnCollider(GameObject trigger, KeyCode keyCode)
    {
        trigger.GetComponent<BoxCollider>().enabled = true;
        pressedBefore = true;
        trigger.GetComponent<Animator>().SetTrigger("IsPressed");
        yield return new WaitForSeconds(.5f);
        if (!Input.GetKey(keyCode))
        {
            pressedBefore = false;
            isPressedDown = false;
            trigger.GetComponent<BoxCollider>().enabled = false;
        }
        else if (trigger.GetComponent<HitScript>().isInteractable)
        {
            pressedBefore = false;
            isPressedDown = true;
        }
        else
        {
            //trigger.GetComponent<BoxCollider>().enabled = false;
            pressedBefore = false;
            isPressedDown = false;
        }
    }
}
