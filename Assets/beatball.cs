﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class beatball : MonoBehaviour {
    public float speed = 20;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        float translation = Time.deltaTime * speed;
        transform.Translate(0, -translation, 0);
    }
}
