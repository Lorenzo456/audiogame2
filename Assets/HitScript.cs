﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitScript : MonoBehaviour {

    public KeyCode userKey;

    public bool isInteractable;
    public bool isPressed;
    public bool isPressedDown;

    public bool checkingPressedDown;
    BoxCollider boxCollider;
    Animator animator;
    float timeLeft = 2;

    private void Start()
    {
        boxCollider = GetComponent<BoxCollider>();
        animator = GetComponent<Animator>();
    }
    private void Update()
    {        
        if (Input.GetKeyUp(userKey))
        {
            //ResetInput();
            isPressed = false;
            isPressedDown = false;
            boxCollider.enabled = false;
            timeLeft = 2;
        }

        if (Input.GetKeyDown(userKey) && timeLeft > 0)
        {
            isPressed = true;
            boxCollider.enabled = true;
            animator.SetTrigger("IsPressed");
            Debug.Log(isPressed);
            StartCoroutine("IsPressedEnum");
        }


        Vector3 fwd = transform.TransformDirection(Vector3.up);
      //  Debug.DrawRay(transform.position, fwd * 10, Color.green);
     //   Debug.Log(Physics.Raycast(transform.position, fwd, 10));
        if (isPressedDown)
        {
            if (!Physics.Raycast(transform.position, fwd, 10))
            {
                if (timeLeft > 0)
                {
                    timeLeft -= Time.deltaTime;
                }
                else
                {
                    isPressed = false;
                    isPressedDown = false;
                    boxCollider.enabled = false;
           //         Debug.Log("NOTPRESSEDDOWN");
                }
            }
        }
        else
        {
            
            timeLeft = 2;
        }

    }

    void ResetInput()
    {
       // Debug.Log("RESET");
        StopAllCoroutines();
        isPressed = false;
        isPressedDown = false;
        checkingPressedDown = false;
    }

    IEnumerator IsPressedEnum()
    {
        yield return new WaitForSeconds(0.2f);
        if (Input.GetKey(userKey) && timeLeft > 0)
        {
         //   Debug.Log("PressedDown");
         //   Debug.Log(isPressedDown);
            isPressedDown = true;
            isPressed = false;
        }
        else
        {
            isPressed = false;
            boxCollider.enabled = false;
        }
    }

    /*
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Interactable"))
        {
            isInteractable = true;
        }
        else
        {
            isInteractable = false;
        }

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Interactable"))
        {
            isInteractable = false;
        }
    }
    */
}
