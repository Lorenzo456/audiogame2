﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeatSpawner : MonoBehaviour, NoteInteraction {

    public GameObject beatBall;
    bool beat;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (beat)
        {
            GameObject tempBeatbeatBall = ObjectPool.Instance.GetPooledObject();
            tempBeatbeatBall.transform.position = transform.position;
            tempBeatbeatBall.SetActive(true);
            //Instantiate(beatBall, transform.position, Quaternion.identity);
            //beatBall.transform.position = transform.position;
            beat = false;
        }
    }

    public void Interaction()
    {
        Debug.Log("SPAWNED");
        beat = true;
    }

    public void InteractionOff()
    {
        
    }
}
